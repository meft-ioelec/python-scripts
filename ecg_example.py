# -*- coding: utf-8 -*-
"""
Created on Wed Oct 21 12:27:14 2020
The returned signal is a 5 minute long electrocardiogram (ECG), 
 a medical recording of the heart’s electrical activity, sampled at 360 Hz.

@author: bernardo
"""
from scipy.misc import electrocardiogram
from scipy.fft import fft
from scipy import signal # 
import matplotlib.pyplot as plt
import numpy as np

ecg = electrocardiogram()

fs = 360
time = np.arange(ecg.size) / fs


# FFT
# time interval, 4s 
Dt=4
# Number of sample points, 4 s of data
N = fs * Dt 
yf = fft(ecg)
# Frequency axis 0 -> Fs/2
xf = np.linspace(0.0, fs/2, N //2)

fig, (ax1, ax2) = plt.subplots(2,1)
ax1.plot(time, ecg)
ax1.set_xlabel("time / s")
ax1.set_ylabel("ECG / mV")
ax1.set_xlim(0, Dt)
ax1.set_ylim(-1, 2.0)
ax1.set_title("ECG example signal from Scipy")

ax2.plot(xf, 2.0/N * np.abs(yf[0:N//2]), label='FFT of ECG signal')
#ax2.semilogy(xf, 2.0/N * np.abs(yf[0:N//2]))
ax2.set_xlabel("Freq / Hz")
ax2.grid()
ax2.legend()
#ax2.set_title("FFT of ECG signal")
plt.tight_layout()


# export data for ngspice simulation
expdata = np.array([time[0:N-1], ecg[0:N-1]]).transpose()
np.savetxt('ecgdata.txt', expdata, delimiter=' ')

## Digital Notch Filter 50 Hz
noise50=np.sin(2*np.pi*50*time)
ecgnoisy = ecg + 0.4* noise50

f0 = 50.0  # Frequency to be removed from signal (Hz)
Q = 30.0  # Quality factor
# Design notch filter
b, a = signal.iirnotch(f0, Q, fs)
ecgfiltered =  signal.filtfilt(b, a, ecgnoisy)


fig2, (ax1, ax2) = plt.subplots(2,1)
ax1.plot(time, ecgnoisy,  '-b', label='with 50Hz noise')
ax1.set_xlim(0, Dt)
ax1.set_ylim(-1, 2.0)
ax1.set_xlabel("time / s")
ax1.legend()

ax2.plot(time, ecgfiltered,  '-r',  label='filtered')
ax2.set_xlim(0, Dt)
ax2.set_ylim(-1, 2.0)
ax2.set_xlabel("time / s")
ax2.legend()
plt.show()